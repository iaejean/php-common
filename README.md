# PHP Common

[![pipeline status](https://gitlab.com/iaejean/php-common/badges/master/pipeline.svg)](https://gitlab.com/iaejean/php-common/-/commits/master)
[![coverage report](https://gitlab.com/iaejean/php-common/badges/master/coverage.svg)](https://gitlab.com/iaejean/php-common/-/commits/master)

A simple library to wrap some helpers, utils and common components for PHP Development

### Helpers

* Serializer [JMS Serializer](https://jmsyst.com/libs/serializer)
* Deserializer [JMS Serializer](https://jmsyst.com/libs/serializer)
* Validator [Symfony Validator](https://symfony.com/doc/current/validation.html)

### Clients

* Simple Rest Client [Guzzle](http://docs.guzzlephp.org/)
* Simple Soap Client [SoapClient](http://php.net/manual/es/class.soapclient.php)

## Developers guide

### Contribution guidelines ###

Please consider read about some concepts like OOP, Patterns design, TDD, PSR1, PSR2, etc.
 
This project has been provided with several tools for ensure the quality code:

* PHP Code Sniffer
* PHP Mess Detector
* PHP CS Fixer
```shell script
vendor/bin/phpcbf --standard=PSR2 src/ tests/
vendor/bin/phpcbf --standard=PSR1 src/ tests/

vendor/bin/phpmd src/ xml controversial design naming unusedcode --exclude=vendor/
vendor/bin/phpmd tests/ xml codesize controversial design naming unusedcode --exclude=vendor/

vendor/bin/php-cs-fixer fix --diff --dry-run 
```

* PHPMetrics
* PHPLoc
```shell script
vendor/bin/phploc src
vendor/bin/phpmetrics --report-html=report/metrics ./
```

* PHPUnit
```shell script
vendor/bin/phpunit -c phpunit.xml.dist
```
### Gitlab Runner

```shell script
$ brew install gitlab-runner 
#or
$ sudo apt-get install gitlab-runner

$ gitlab-runner register

$ vim ~/.gitlab-runner/config.toml

privileged = true

$ gitlab-runner exec docker test --docker-privileged
$ gitlab-runner exec docker deploy --docker-privileged
```


```shell script
$ brew install gitlab-runner
$ brew services start gitlab-runner
$ gitlab-runner register
$ gitlab-runner exec docker test
```

## @Todo list
* Support multiple drivers like Symfony HttpClient or Guzzle
* Add Support PSR-7, PSR-18
* Add missing tests
