<?php

declare(strict_types=1);

namespace Iaejean\Common\Contracts\Helpers;

use Iaejean\Common\Helpers\Exceptions\InvalidArgumentException;
use Iaejean\Common\Helpers\Exceptions\ValidationException;

/**
 * Interface ValidatorHelperInterface
 * @package Iaejean\Common\Contracts\Helpers
 */
interface ValidatorHelperInterface
{
    /**
     * @param object $instance
     * @param array|null $groups
     * @throws ValidationException
     * @throws InvalidArgumentException
     */
    public static function validate(object $instance, ?array $groups = null): void;
}
