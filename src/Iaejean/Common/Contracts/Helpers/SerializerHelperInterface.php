<?php

declare(strict_types=1);

namespace Iaejean\Common\Contracts\Helpers;

use Iaejean\Common\Helpers\Exceptions\InvalidArgumentException;
use JMS\Serializer\SerializationContext;

/**
 * Interface SerializerHelperInterface
 * @package Iaejean\Common\Contracts\Helpers
 */
interface SerializerHelperInterface
{
    /**
     * @param $object
     * @param SerializationContext|null $context
     * @throws InvalidArgumentException
     * @return string
     */
    public static function toJson($object, ?SerializationContext $context = null): string;

    /**
     * @param $object
     * @param SerializationContext|null $context
     * @throws InvalidArgumentException
     * @return string
     */
    public static function toXml($object, ?SerializationContext $context = null): string;
}
