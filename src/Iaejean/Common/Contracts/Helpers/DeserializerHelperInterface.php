<?php

declare(strict_types=1);

namespace Iaejean\Common\Contracts\Helpers;

use Iaejean\Common\Helpers\Exceptions\InvalidArgumentException;

/**
 * Interface DeserializerHelperInterface
 * @package Iaejean\Common\Contracts\Helpers
 */
interface DeserializerHelperInterface
{
    /**
     * @param string $json
     * @param string $className
     * @throws InvalidArgumentException
     * @return object
     */
    public static function parseJson(string $json, string $className = 'stdClass'): object;

    /**
     * @param string $xml
     * @param string $className
     * @throws InvalidArgumentException
     * @return object
     */
    public static function parseXml(string $xml, string $className): object;
}
