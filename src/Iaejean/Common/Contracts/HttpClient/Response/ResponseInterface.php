<?php

declare(strict_types=1);

namespace Iaejean\Common\Contracts\HttpClient\Response;

/**
 * Interface ResponseInterface
 * @package Iaejean\Common\Contracts\HttpClient\Response
 */
interface ResponseInterface
{
}
