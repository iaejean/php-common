<?php

declare(strict_types=1);

namespace Iaejean\Common\Contracts\HttpClient;

use Iaejean\Common\HttpClient\Exceptions\CommunicationException;
use Iaejean\Common\HttpClient\Exceptions\RestException;
use Iaejean\Common\HttpClient\Exceptions\SoapException;
use Iaejean\Common\HttpClient\Request\RestRequest;
use Iaejean\Common\HttpClient\Request\SoapRequest;
use Iaejean\Common\HttpClient\Response\Response;

/**
 * Interface HttpClientInterface
 * @package Iaejean\Common\Contracts\HttpClient
 */
interface HttpClientInterface
{
    /**
     * @param RestRequest $request
     * @throws CommunicationException
     * @throws RestException
     * @return Response
     */
    public function get(RestRequest $request): Response;

    /**
     * @param RestRequest $request
     * @throws CommunicationException
     * @throws RestException
     * @return Response
     */
    public function post(RestRequest $request): Response;

    /**
     * @param RestRequest $request
     * @throws CommunicationException
     * @throws RestException
     * @return Response
     */
    public function delete(RestRequest $request): Response;

    /**
     * @param RestRequest $request
     * @throws CommunicationException
     * @throws RestException
     * @return Response
     */
    public function put(RestRequest $request): Response;

    /**
     * @param SoapRequest $request
     * @throws CommunicationException
     * @throws SoapException
     * @return Response
     */
    public function call(SoapRequest $request): Response;
}
