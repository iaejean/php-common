<?php

declare(strict_types=1);

namespace Iaejean\Common\Contracts\HttpClient;

/**
 * Interface ToArrayInterface
 * @package Iaejean\Common\Contracts\HttpClient
 */
interface ToArrayInterface
{
    /**
     * @param bool $fromHelper
     * @return array
     */
    public function toArray(bool $fromHelper = true): array;
}
