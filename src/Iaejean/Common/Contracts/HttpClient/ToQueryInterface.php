<?php

declare(strict_types=1);

namespace Iaejean\Common\Contracts\HttpClient;

/**
 * Interface ToQueryInterface
 * @package Iaejean\Common\Contracts\HttpClient
 */
interface ToQueryInterface
{
    /**
     * @param bool $fromHelper
     * @return string
     */
    public function toQuery(bool $fromHelper = true): string;
}
