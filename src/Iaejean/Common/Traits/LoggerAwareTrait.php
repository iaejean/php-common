<?php

declare(strict_types=1);

namespace Iaejean\Common\Traits;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Trait LoggerAwareTrait
 * @package Iaejean\Common\Traits
 */
trait LoggerAwareTrait
{
    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $logger;

    /**
     * @param LoggerInterface|null $logger
     * @return $this
     */
    public function setLogger(?LoggerInterface $logger = null): self
    {
        $this->logger = $logger ?? new NullLogger();

        return $this;
    }
}
