<?php

declare(strict_types=1);

namespace Iaejean\Common\Traits;

use Iaejean\Common\Helpers\SerializerHelper;

/**
 * Trait ToQueryTrait
 * @package Iaejean\Common\Traits
 */
trait ToQueryTrait
{
    /**
     * {@inheritdoc}
     */
    public function toQuery(bool $fromHelper = true): string
    {
        if ($fromHelper) {
            $json = SerializerHelper::toJSON($this);
            $attributes = json_decode($json, true);

            return sprintf('?%s', http_build_query($attributes));
        }

        return sprintf('?%s', http_build_query(get_object_vars($this)));
    }
}
