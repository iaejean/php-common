<?php

declare(strict_types=1);

namespace Iaejean\Common\Traits;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\CachedReader;
use Doctrine\Common\Annotations\Reader;
use Doctrine\Common\Cache\FilesystemCache;
use Iaejean\Common\Helpers\Exceptions\InvalidArgumentException;
use JMS\Serializer\SerializerBuilder;

/**
 * Trait CheckCacheDirectoryTrait
 * @package Iaejean\Common\Traits0
 */
trait CheckCacheDirectoryTrait
{
    /**
     * @var string|null
     */
    public static ?string $cacheDir = null;

    /**
     * @param SerializerBuilder|null $handler
     * @return Reader|null
     */
    private static function checkCacheDirectory($handler = null): ?Reader
    {
        $directory = self::$cacheDir;
        if (null !== $directory) {
            if (file_exists($directory) && is_dir($directory) && is_writable($directory)) {
                if ($handler instanceof SerializerBuilder) {
                    $handler->setCacheDir($directory.'/jms_serializer');
                }
                if (null === $handler) {
                    return new CachedReader(new AnnotationReader(), new FilesystemCache($directory.'/validator'), true);
                }
            } else {
                throw new InvalidArgumentException($directory.' is unavailable to cache metadata');
            }
        }

        return null;
    }
}
