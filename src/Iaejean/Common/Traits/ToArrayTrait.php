<?php

declare(strict_types=1);

namespace Iaejean\Common\Traits;

use Iaejean\Common\Helpers\SerializerHelper;

/**
 * Trait ToArrayTrait
 * @package Iaejean\Common\Traits
 */
trait ToArrayTrait
{
    /**
     * {@inheritdoc}
     */
    public function toArray(bool $fromHelper = true): array
    {
        if ($fromHelper) {
            $json = SerializerHelper::toJSON($this);

            return json_decode($json, true);
        }

        return get_object_vars($this);
    }
}
