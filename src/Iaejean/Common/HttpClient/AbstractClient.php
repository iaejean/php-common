<?php

declare(strict_types=1);

namespace Iaejean\Common\HttpClient;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Psr7\Response as PSRResponse;
use GuzzleHttp\RequestOptions;
use Iaejean\Common\Contracts\HttpClient\HttpClientInterface;
use Iaejean\Common\Contracts\HttpClient\Request\RequestInterface;
use Iaejean\Common\Helpers\Exceptions\InvalidArgumentException;
use Iaejean\Common\HttpClient\Exceptions\CommunicationException;
use Iaejean\Common\HttpClient\Exceptions\RestException;
use Iaejean\Common\HttpClient\Exceptions\SoapException;
use Iaejean\Common\HttpClient\Request\Multipart;
use Iaejean\Common\HttpClient\Request\RestRequest;
use Iaejean\Common\HttpClient\Request\SoapRequest;
use Iaejean\Common\HttpClient\Response\Response;
use Iaejean\Common\Traits\LoggerAwareTrait;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;

/**
 * Class AbstractClient
 * @package Iaejean\Common\HttpClient
 */
abstract class AbstractClient implements HttpClientInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    protected const HTTP_GET = 'GET';
    protected const HTTP_POST = 'POST';
    protected const HTTP_DELETE = 'DELETE';
    protected const HTTP_PUT = 'PUT';

    /**
     * @var string
     */
    protected string $prefix = 'HTTP_CLIENT';

    /**
     * @var bool
     */
    protected bool $cacheEnable;

    /**
     * AbstractClient constructor.
     * @param string $prefix
     * @param LoggerInterface|null $logger
     * @param bool $cacheEnable
     */
    public function __construct(string $prefix, ?LoggerInterface $logger = null, bool $cacheEnable = true)
    {
        $this->prefix = $prefix;
        $this->setLogger($logger);
        $this->cacheEnable = $cacheEnable;
    }

    /**
     * @param RequestInterface $request
     * @param string $method
     * @throws CommunicationException
     * @throws RestException
     * @throws SoapException
     * @return Response
     */
    protected function request(RequestInterface $request, string $method = AbstractClient::HTTP_GET): Response
    {
        if ($request instanceof RestRequest) {
            return $this->restRequest($request, mb_strtolower($method));
        }
        if ($request instanceof SoapRequest) {
            return $this->soapRequest($request);
        }
        throw new InvalidArgumentException('Invalid Request, there isn\'t implementation for this RequestInterface');
    }

    /**
     * @param SoapRequest $request
     * @throws SoapException
     * @return Response
     */
    protected function soapRequest(SoapRequest $request): Response
    {
        try {
            $client = new \SoapClient($request->getUrl(), [
                'cache_wsdl' => $this->cacheEnable ? WSDL_CACHE_MEMORY : WSDL_CACHE_NONE,
                'enconding' => 'UTF-8',
                'exceptions' => true,
                'soap_version' => SOAP_1_2,
                'trace' => true,
                'verifyhost' => false,
            ]);
            $this->logger->info(
                sprintf(
                    '[%s][REQUEST] %s, %s',
                    $this->prefix,
                    $request->getOperation(),
                    json_encode($request->getBody())
                )
            );

            $response = $client->__soapCall($request->getOperation(), $request->getBody());
            $this->logger->info(
                sprintf('[%s][RESPONSE] %s, %s', $this->prefix, $request->getOperation(), json_encode($response))
            );

            return new Response($response, 200);
        } catch (\Exception $exception) {
            $this->logger->error(sprintf('[%s][ERROR] %s', $this->prefix, $exception->getMessage()));
            throw new SoapException($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * @param RestRequest $request
     * @param string $method
     * @throws CommunicationException
     * @throws RestException
     * @return Response
     */
    protected function restRequest(RestRequest $request, string $method = AbstractClient::HTTP_GET): Response
    {
        $url = $this->buildUrl($request);
        $defaults = $this->buildDefaultOptions($request);

        try {
            $this->logger->info(sprintf('[%s][REQUEST][%s] %s %s', $this->prefix, $method, $url, $request->getBody()));
            $guzzleClient = new GuzzleClient();
            /** @var PSRResponse $response */
            $response = $guzzleClient->$method($url, $defaults);
            $this->logger->info(
                sprintf('[%s][RESPONSE][%s] %s %s', $this->prefix, $method, $url, (string) $response->getBody())
            );

            return new Response((string) $response->getBody(), $response->getStatusCode(), $response->getHeaders());
        } catch (ClientException | ServerException $exception) {
            $response = $exception->getResponse();
            $this->logger->error(
                sprintf(
                    '[%s][ERROR][%s] %s %s %s %s',
                    $this->prefix,
                    $method,
                    $response->getStatusCode(),
                    $url,
                    (string) $response->getBody(),
                    $exception->getMessage()
                )
            );
            throw new CommunicationException((string) $response->getBody(), $response->getStatusCode());
        } catch (\Exception $exception) {
            $this->logger->error(
                sprintf('[%s][ERROR][%s] %s %s', $this->prefix, $method, $url, $exception->getMessage())
            );
            throw new RestException($exception->getMessage());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function post(RestRequest $request): Response
    {
        return $this->request($request, AbstractClient::HTTP_POST);
    }

    /**
     * {@inheritdoc}
     */
    public function get(RestRequest $request): Response
    {
        return $this->request($request, AbstractClient::HTTP_GET);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(RestRequest $request): Response
    {
        return $this->request($request, AbstractClient::HTTP_DELETE);
    }

    /**
     * {@inheritdoc}
     */
    public function put(RestRequest $request): Response
    {
        return $this->request($request, AbstractClient::HTTP_PUT);
    }

    /**
     * {@inheritdoc}
     */
    public function call(SoapRequest $request): Response
    {
        return $this->request($request);
    }

    /**
     * @param RestRequest $request
     * @return string
     */
    private function buildUrl(RestRequest $request): string
    {
        $url = $request->getUrl();

        if (null !== $request->getPath()) {
            $url .= ('/' === substr($request->getPath(), 0, 1)) ? $request->getPath() : '/'.$request->getPath();
        }

        return $url;
    }

    /**
     * @param RestRequest $request
     * @throws RestException
     * @return array
     */
    private function buildDefaultOptions(RestRequest $request): array
    {
        $defaults = [
            RequestOptions::VERIFY => $request->isVerify(),
            RequestOptions::HEADERS => $request->getHeaders() ?? [],
        ];

        if (null !== $request->getBody()) {
            $defaults[RequestOptions::BODY] = $request->getBody();
        }

        if (null !== $request->getCookieJar()) {
            $defaults[RequestOptions::COOKIES] = $request->getCookieJar();
        }

        if (!empty($request->getRedirectConfig())) {
            $defaults[RequestOptions::ALLOW_REDIRECTS] = $request->getRedirectConfig();
        }

        if (!empty($request->getMultipart())) {
            unset($defaults[RequestOptions::HEADERS]['Content-Type']);
            unset($defaults[RequestOptions::HEADERS]['content-type']);

            $defaults[RequestOptions::MULTIPART] = [];
            foreach ($request->getMultipart() as $multipart) {
                if (!$multipart instanceof Multipart) {
                    throw new RestException(sprintf('multipart is not a instance of: %s', Multipart::class));
                }
                $item = [
                    'name' => $multipart->getName(),
                    'contents' => $multipart->getContents(),
                ];

                if (null !== $multipart->getFilename()) {
                    $item['contents'] = fopen($multipart->getContents(), 'r');
                    $item['filename'] = $multipart->getFilename();
                }

                $defaults[RequestOptions::MULTIPART][] = $item;
            }
        }

        if (null !== $request->getAuth()) {
            $defaults[RequestOptions::AUTH] = $request->getAuth();
        }

        return $defaults;
    }
}
