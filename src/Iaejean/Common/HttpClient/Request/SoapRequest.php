<?php

declare(strict_types=1);

namespace Iaejean\Common\HttpClient\Request;

use Iaejean\Common\Contracts\HttpClient\Request\RequestInterface;

/**
 * Class SoapRequest
 * @package Iaejean\Common\HttpClient\Request
 */
class SoapRequest implements RequestInterface
{
    /**
     * @var string
     */
    private string $url;

    /**
     * @var string
     */
    private string $operation;

    /**
     * @var mixed
     */
    private $body;

    /**
     * SoapRequest constructor.
     * @param string $url
     * @param string $operation
     * @param $body
     */
    public function __construct(string $url, string $operation, $body)
    {
        $this->url = $url;
        $this->operation = $operation;
        $this->body = $body;
    }

    /**
     * @return string
     */
    public function getOperation(): string
    {
        return $this->operation;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }
}
