<?php

declare(strict_types=1);

namespace Iaejean\Common\HttpClient\Request;

/**
 * Class Multipart
 * @package Iaejean\Common\HttpClient\Request
 */
class Multipart
{
    /**
     * @var string
     */
    protected string $name;

    /**
     * @var string|null
     */
    protected ?string $contents;

    /**
     * @var string|null
     */
    protected ?string $filename;

    /**
     * Multipart constructor.
     * @param string $name
     * @param string|null $contents
     * @param string|null $filename
     */
    public function __construct(string $name, ?string $contents = null, ?string $filename = null)
    {
        $this->name = $name;
        $this->contents = $contents;
        $this->filename = $filename;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getContents(): ?string
    {
        return $this->contents;
    }

    /**
     * @return string|null
     */
    public function getFilename(): ?string
    {
        return $this->filename;
    }
}
