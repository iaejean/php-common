<?php

declare(strict_types=1);

namespace Iaejean\Common\HttpClient\Request;

use GuzzleHttp\Cookie\CookieJarInterface;
use Iaejean\Common\Contracts\HttpClient\Request\RequestInterface;

/**
 * Class RestRequest
 * @package Iaejean\Common\HttpClient\Request
 */
class RestRequest implements RequestInterface
{
    /**
     * @var string|null
     */
    private ?string $path = null;

    /**
     * @var array|null
     */
    private ?array $headers = null;

    /**
     * @var bool
     */
    private bool $verify = false;

    /**
     * @var array|null
     */
    private ?array $auth = null;

    /**
     * @var string
     */
    private string $url;

    /**
     * @var mixed
     */
    private $body;

    /**
     * @var array Multipart[]
     */
    private array $multipart = [];

    /**
     * @var CookieJarInterface|null
     */
    private ?CookieJarInterface $cookieJar = null;

    /**
     * @var array
     */
    private array $redirectConfig;

    /**
     * @var \Closure|null
     */
    private ?\Closure $onStats = null;

    /**
     * RestRequest constructor.
     * @param string $url
     * @param string|null $path
     * @param null $body
     * @param array|null $headers
     * @param bool $verify
     * @param array|null $auth
     * @param array $multipart
     * @param CookieJarInterface|null $cookieJar
     * @param array $redirectConfig
     * @param \Closure|null $onStats
     */
    public function __construct(
        string $url,
        ?string $path = null,
        $body = null,
        ?array $headers = null,
        bool $verify = false,
        ?array $auth = [],
        array $multipart = [],
        ?CookieJarInterface $cookieJar = null,
        array $redirectConfig = [],
        ?\Closure $onStats = null
    ) {
        $this->path = $path;
        $this->headers = $headers;
        $this->verify = $verify;
        $this->auth = $auth;
        $this->url = $url;
        $this->body = $body;
        $this->multipart = $multipart;
        $this->cookieJar = $cookieJar;
        $this->redirectConfig = $redirectConfig;
        $this->onStats = $onStats;
    }

    /**
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * @param string|null $path
     * @return RestRequest
     */
    public function setPath(?string $path): RestRequest
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getHeaders(): ?array
    {
        return $this->headers;
    }

    /**
     * @param array|null $headers
     * @return RestRequest
     */
    public function setHeaders(?array $headers): RestRequest
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * @return bool
     */
    public function isVerify(): bool
    {
        return $this->verify;
    }

    /**
     * @param bool $verify
     * @return RestRequest
     */
    public function setVerify(bool $verify): RestRequest
    {
        $this->verify = $verify;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getAuth(): ?array
    {
        return $this->auth;
    }

    /**
     * @param array|null $auth
     * @return RestRequest
     */
    public function setAuth(?array $auth): RestRequest
    {
        $this->auth = $auth;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return RestRequest
     */
    public function setUrl(string $url): RestRequest
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param mixed $body
     * @return RestRequest
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @return array
     */
    public function getMultipart(): array
    {
        return $this->multipart;
    }

    /**
     * @param array $multipart
     * @return RestRequest
     */
    public function setMultipart(array $multipart): RestRequest
    {
        $this->multipart = $multipart;
        return $this;
    }

    /**
     * @return CookieJarInterface|null
     */
    public function getCookieJar(): ?CookieJarInterface
    {
        return $this->cookieJar;
    }

    /**
     * @param CookieJarInterface|null $cookieJar
     * @return RestRequest
     */
    public function setCookieJar(?CookieJarInterface $cookieJar): RestRequest
    {
        $this->cookieJar = $cookieJar;
        return $this;
    }

    /**
     * @return array
     */
    public function getRedirectConfig(): array
    {
        return $this->redirectConfig;
    }

    /**
     * @param array $redirectConfig
     * @return RestRequest
     */
    public function setRedirectConfig(array $redirectConfig): RestRequest
    {
        $this->redirectConfig = $redirectConfig;
        return $this;
    }

    /**
     * @return \Closure|null
     */
    public function getOnStats(): ?\Closure
    {
        return $this->onStats;
    }

    /**
     * @param \Closure|null $onStats
     * @return RestRequest
     */
    public function setOnStats(?\Closure $onStats): RestRequest
    {
        $this->onStats = $onStats;
        return $this;
    }
}
