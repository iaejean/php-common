<?php

declare(strict_types=1);

namespace Iaejean\Common\HttpClient\Response;

use Iaejean\Common\Contracts\HttpClient\Response\ResponseInterface;

/**
 * Class Response
 * @package Iaejean\Common\HttpClient\Response
 */
class Response implements ResponseInterface
{
    /**
     * @var mixed
     */
    private $body;

    /**
     * @var int
     */
    private int $code;

    /**
     * @var array
     */
    private array $headers;

    /**
     * Response constructor.
     * @param $body
     * @param int $code
     * @param array $headers
     */
    public function __construct($body, int $code = 0, $headers = [])
    {
        $this->body = $body;
        $this->code = $code;
        $this->headers = $headers;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }
}
