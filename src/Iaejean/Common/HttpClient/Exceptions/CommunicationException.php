<?php

declare(strict_types=1);

namespace Iaejean\Common\HttpClient\Exceptions;

/**
 * Class CommunicationException
 * @package Iaejean\Common\HttpClient\Exceptions
 */
class CommunicationException extends \Exception
{
    private const CODE = 200;

    /**
     * CommunicationException constructor.
     * @param string $message
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct(string $message, int $code = self::CODE, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
