<?php

declare(strict_types=1);

namespace Iaejean\Common\HttpClient\Exceptions;

/**
 * Class SoapException
 * @package Iaejean\Common\HttpClient\Exceptions
 */
class SoapException extends \Exception
{
    private const CODE = 400;

    /**
     * SoapException constructor.
     * @param string $message
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct(string $message, int $code = self::CODE, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
