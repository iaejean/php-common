<?php

declare(strict_types=1);

namespace Iaejean\Common\Helpers;

use Iaejean\Common\Contracts\Helpers\DeserializerHelperInterface;
use Iaejean\Common\Helpers\Exceptions\InvalidArgumentException;
use Iaejean\Common\Traits\CheckCacheDirectoryTrait;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use JMS\Serializer\Naming\SerializedNameAnnotationStrategy;
use JMS\Serializer\SerializerBuilder;

/**
 * Class DeserializerHelper
 * @package Iaejean\Common\Helpers
 */
final class DeserializerHelper implements DeserializerHelperInterface
{
    use CheckCacheDirectoryTrait;

    /**
     * {@inheritdoc}
     */
    public static function parseJson(string $json, string $className = 'stdClass'): object
    {
        self::validateJson($json);

        if ('stdClass' === $className) {
            return json_decode($json);
        }

        $serializerBuilder = SerializerBuilder::create();
        $serializerBuilder->setPropertyNamingStrategy(
            new SerializedNameAnnotationStrategy(new IdenticalPropertyNamingStrategy())
        );

        self::checkCacheDirectory($serializerBuilder);
        $serializer = $serializerBuilder->build();

        return $serializer->deserialize($json, $className, 'json');
    }

    /**
     * {@inheritdoc}
     */
    public static function parseXml(string $xml, string $className): object
    {
        self::validateXml($xml);

        $serializerBuilder = SerializerBuilder::create();
        self::checkCacheDirectory($serializerBuilder);
        $serializer = $serializerBuilder->build();

        return $serializer->deserialize($xml, $className, 'xml');
    }

    /**
     * @param string $json
     * @throws InvalidArgumentException
     */
    private static function validateJson(string $json): void
    {
        try {
            json_decode($json, $assoc = false, $depth = 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException $jsonException) {
            throw new InvalidArgumentException($jsonException->getMessage(), $jsonException->getCode(), $jsonException);
        }
    }

    /**
     * @param string $xml
     * @throws InvalidArgumentException
     */
    private static function validateXml(string $xml): void
    {
        libxml_use_internal_errors(true);

        $xml = simplexml_load_string($xml);
        if (!$xml) {
            libxml_clear_errors();
            throw new InvalidArgumentException('Invalid XML: '.$xml);
        }
    }
}
