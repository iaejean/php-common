<?php

declare(strict_types=1);

namespace Iaejean\Common\Helpers;

use Doctrine\Common\Annotations\AnnotationReader;
use Iaejean\Common\Contracts\Helpers\ValidatorHelperInterface;
use Iaejean\Common\Helpers\Exceptions\ValidationException;
use Iaejean\Common\Traits\CheckCacheDirectoryTrait;
use Symfony\Component\Validator\Validation;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ValidatorHelper
 * @package Iaejean\Common\Helpers
 */
final class ValidatorHelper implements ValidatorHelperInterface
{
    use CheckCacheDirectoryTrait;

    /**
     * @var TranslatorInterface|null
     */
    public static ?TranslatorInterface $translator = null;

    /**
     * {@inheritdoc}
     */
    public static function validate(object $instance, ?array $groups = null): void
    {
        $reader = self::checkCacheDirectory() ?? new AnnotationReader();
        $validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping($reader)
            ->setTranslationDomain('validators');

        if (ValidatorHelper::$translator instanceof TranslatorInterface) {
            $validator->setTranslator(ValidatorHelper::$translator);
        }

        $validator = $validator->getValidator();
        $violations = $validator->validate($instance, null, $groups);

        if ($violations->count() > 0) {
            $msg = 'Invalid arguments for: ';
            foreach ($violations as $error) {
                $msg .= $error->getPropertyPath();
                $msg .= ' ';
                $msg .= $error->getMessage();
                $msg .= '; ';
            }
            throw new ValidationException($msg, $violations);
        }
    }
}
