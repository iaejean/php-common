<?php

declare(strict_types=1);

namespace Iaejean\Common\Helpers;

use Iaejean\Common\Contracts\Helpers\SerializerHelperInterface;
use Iaejean\Common\Helpers\Exceptions\InvalidArgumentException;
use Iaejean\Common\Traits\CheckCacheDirectoryTrait;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\Visitor\Factory\JsonSerializationVisitorFactory;
use JMS\Serializer\Visitor\Factory\XmlSerializationVisitorFactory;

/**
 * Class SerializerHelper
 * @package Iaejean\Common\Helpers
 */
final class SerializerHelper implements SerializerHelperInterface
{
    use CheckCacheDirectoryTrait;

    /**
     * {@inheritdoc}
     */
    public static function toJson($object, ?SerializationContext $context = null): string
    {
        if ($object instanceof \stdClass) {
            try {
                return json_encode($object, JSON_THROW_ON_ERROR);
            } catch (\JsonException $jsonException) {
                throw new InvalidArgumentException($jsonException->getMessage(), $jsonException->getCode(), $jsonException);
            }
        }

        $serializationVisitorFactory = new JsonSerializationVisitorFactory();
        $serializationVisitorFactory->setOptions(JSON_THROW_ON_ERROR);

        $serializerBuilder = SerializerBuilder::create();
        $serializerBuilder->setSerializationVisitor('json', $serializationVisitorFactory)
            ->setPropertyNamingStrategy(new IdenticalPropertyNamingStrategy());
        self::checkCacheDirectory($serializerBuilder);
        $serializer = $serializerBuilder->build();

        return $serializer->serialize($object, 'json', $context);
    }

    /**
     * {@inheritdoc}
     */
    public static function toXml($object, ?SerializationContext $context = null): string
    {
        $serializationVisitorFactory = new XmlSerializationVisitorFactory();
        $serializerBuilder = SerializerBuilder::create();
        $serializerBuilder->setSerializationVisitor('xml', $serializationVisitorFactory)
            ->setPropertyNamingStrategy(new IdenticalPropertyNamingStrategy());
        self::checkCacheDirectory($serializerBuilder);
        $serializer = $serializerBuilder->build();

        return $serializer->serialize($object, 'xml', $context);
    }
}
