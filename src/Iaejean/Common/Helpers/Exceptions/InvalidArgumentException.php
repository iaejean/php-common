<?php

declare(strict_types=1);

namespace Iaejean\Common\Helpers\Exceptions;

/**
 * Class InvalidArgumentException
 * @package Iaejean\Common\Helpers\Exceptions
 */
class InvalidArgumentException extends \InvalidArgumentException
{
    private const CODE = 300;

    /**
     * InvalidArgumentException constructor.
     * @param string $message
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct(string $message, int $code = self::CODE, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
