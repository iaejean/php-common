<?php

declare(strict_types=1);

namespace Iaejean\Common\Helpers\Exceptions;

use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Class ValidationException
 * @package Iaejean\Common\Helpers\Exceptions
 */
class ValidationException extends \Exception
{
    private const CODE = 500;

    /**
     * @var ConstraintViolationListInterface|null
     */
    private ?ConstraintViolationListInterface $violations;

    /**
     * ValidationException constructor.
     * @param string $message
     * @param ConstraintViolationListInterface|null $violations
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct(
        string $message,
        ?ConstraintViolationListInterface $violations = null,
        int $code = self::CODE,
        \Exception $previous = null
    ) {
        $this->violations = $violations;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return ConstraintViolationListInterface|null
     */
    public function getViolations(): ?ConstraintViolationListInterface
    {
        return $this->violations;
    }
}
