<?php

declare(strict_types=1);

namespace Tests\Iaejean\Common\HttpClient;

use Iaejean\Common\Contracts\HttpClient\Response\ResponseInterface;
use Iaejean\Common\HttpClient\Exceptions\CommunicationException;
use Iaejean\Common\HttpClient\Exceptions\RestException;
use Iaejean\Common\HttpClient\Response\Response;
use PHPUnit\Framework\TestCase;
use Tests\Iaejean\Common\Resources\MySoapClient;
use Tests\Iaejean\Common\Resources\RestClient;
use Tests\Iaejean\Common\TraitTest;

/**
 * Class TestClient
 * @package Tests\Iaejean\Common\HttpClient
 */
class TestClient extends TestCase
{
    /**
     * @var RestClient
     */
    private static RestClient $restClient;

    /**
     * @var MySoapClient MySoapClient
     */
    private static MySoapClient $soapClient;

    use TraitTest {
        TraitTest::setupBeforeClass as TraitSetupBeforeClass;
    }

    /**
     * @throws \Exception
     */
    public static function setupBeforeClass(): void
    {
        self::TraitSetupBeforeClass();
        self::$restClient = new RestClient('rest', self::$logger);
        self::$soapClient = new MySoapClient('soap', self::$logger);
    }

    /**
     * @test
     */
    public function canCall(): void
    {
        try {
            $response = self::$soapClient->getCall();
            self::assertInstanceOf(Response::class, $response);
            self::assertInstanceOf(ResponseInterface::class, $response);
        } catch (\Exception $exception) {
            self::assertTrue(true);
        }
    }

    /**
     * @test
     * @throws CommunicationException
     * @throws RestException
     */
    public function canGet(): void
    {
        $response = self::$restClient->getPosts();
        self::assertInstanceOf(Response::class, $response);
        self::assertInstanceOf(ResponseInterface::class, $response);
    }

    /**
     * @test
     * @throws CommunicationException
     * @throws RestException
     */
    public function canPost(): void
    {
        $response = self::$restClient->postPosts();
        self::assertInstanceOf(Response::class, $response);
        self::assertInstanceOf(ResponseInterface::class, $response);
    }

    /**
     * @test
     * @throws CommunicationException
     * @throws RestException
     */
    public function canPostMultipart(): void
    {
        $response = self::$restClient->postPostMultipart();
        self::assertInstanceOf(Response::class, $response);
        self::assertInstanceOf(ResponseInterface::class, $response);
    }

    /**
     * @test
     * @throws CommunicationException
     * @throws RestException
     */
    public function canDelete(): void
    {
        $response = self::$restClient->putPost();
        self::assertInstanceOf(Response::class, $response);
        self::assertInstanceOf(ResponseInterface::class, $response);
    }

    /**
     * @test
     * @throws CommunicationException
     * @throws RestException
     */
    public function canPut(): void
    {
        $response = self::$restClient->deletePost();
        self::assertInstanceOf(Response::class, $response);
        self::assertInstanceOf(ResponseInterface::class, $response);
    }
}
