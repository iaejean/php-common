<?php

declare(strict_types=1);

namespace Tests\Iaejean\Common\Helpers;

use Iaejean\Common\Helpers\Exceptions\ValidationException;
use Iaejean\Common\Helpers\ValidatorHelper;
use PHPUnit\Framework\TestCase;
use Tests\Iaejean\Common\Resources\Bar;
use Tests\Iaejean\Common\Resources\Dummy;
use Tests\Iaejean\Common\Resources\DummyFull;
use Tests\Iaejean\Common\Resources\Foo;
use Tests\Iaejean\Common\TraitTest;

/**
 * Class TestValidatorHelper
 * @package Tests\Iaejean\Common\Helpers
 */
class TestValidatorHelper extends TestCase
{
    use TraitTest;

    /**
     * @test
     * @dataProvider providerValidObjects
     * @param $object
     * @throws ValidationException
     */
    public function validateOk($object): void
    {
        self::$logger->info('Validating :'.get_class($object));
        ValidatorHelper::validate($object);
        self::assertTrue(true);
    }

    /**
     * @test
     * @dataProvider providerNotValidObjects
     * @param $object
     * @throws ValidationException
     */
    public function validateNotOk($object): void
    {
        self::expectException(ValidationException::class);
        self::$logger->info('Validating :'.get_class($object));
        ValidatorHelper::validate($object);
    }

    /**
     * @return array
     */
    public function providerValidObjects(): array
    {
        $dummy = new Dummy('bar', 'foo');
        $foo = new Foo('foo');
        $bar = new Bar('bar');
        $dummyFull = new DummyFull('foo', $dummy);

        return [
            [json_decode('{}')],
            [json_decode('{"foo":"foo"}')],
            [$dummy],
            [$foo],
            [$bar],
            [$dummyFull],
        ];
    }

    /**
     * @return array
     */
    public function providerNotValidObjects(): array
    {
        $dummy = new Dummy('foo');
        $foo = new Foo();
        $bar = new Bar();
        $dummyFull = new DummyFull('foo', $dummy);

        return [
            [$dummy],
            [$foo],
            [$bar],
            [$dummyFull],
        ];
    }
}
