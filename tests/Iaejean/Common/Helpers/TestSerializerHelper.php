<?php

declare(strict_types=1);

namespace Tests\Iaejean\Common\Helpers;

use Iaejean\Common\Helpers\SerializerHelper;
use PHPUnit\Framework\TestCase;
use Tests\Iaejean\Common\Resources\Bar;
use Tests\Iaejean\Common\Resources\Dummy;
use Tests\Iaejean\Common\Resources\DummyFull;
use Tests\Iaejean\Common\Resources\Foo;
use Tests\Iaejean\Common\TraitTest;

/**
 * Class TestSerializerHelper
 * @package Tests\Iaejean\Common\Helpers
 */
class TestSerializerHelper extends TestCase
{
    use TraitTest;

    /**
     * @param $object
     * @test
     * @dataProvider providerJson
     */
    public function canSerialize($object): void
    {
        self::$logger->info('Serializing :'.get_class($object).' - '.print_r($object, true));
        $json = SerializerHelper::toJSON($object);
        $xml = SerializerHelper::toXml($object);
        self::$logger->info(print_r($json, true));
        self::$logger->info(print_r($xml, true));
        self::assertJson($json);
    }

    /**
     * @return array
     */
    public function providerJson(): array
    {
        $dummy = new Dummy('bar', 'foo');
        $foo = new Foo('foo');
        $bar = new Bar('bar');
        $dummyFull = new DummyFull('foo', $dummy);

        return [
            [json_decode('{}')],
            [json_decode('{"foo":"foo"}')],
            [$dummy],
            [$foo],
            [$bar],
            [$dummyFull],
        ];
    }
}
