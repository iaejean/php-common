<?php

declare(strict_types=1);

namespace Tests\Iaejean\Common\Helpers;

use Iaejean\Common\Helpers\DeserializerHelper;
use Iaejean\Common\Helpers\Exceptions\InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Tests\Iaejean\Common\Resources\Bar;
use Tests\Iaejean\Common\Resources\Dummy;
use Tests\Iaejean\Common\Resources\DummyFull;
use Tests\Iaejean\Common\Resources\Foo;
use Tests\Iaejean\Common\TraitTest;

/**
 * Class TestDeserializerHelper
 * @package Tests\Iaejean\Common\Helpers
 */
class TestDeserializerHelper extends TestCase
{
    use TraitTest;

    /**
     * @param string $noJson
     * @param string $className
     * @test
     * @dataProvider providerNoJson
     */
    public function cantDeserializeNoJson(string $noJson, string $className): void
    {
        self::expectException(InvalidArgumentException::class);

        self::$logger->info('Deserializing: '.$className.' - '.print_r($noJson, true));
        DeserializerHelper::parseJSON($noJson);
    }

    /**
     * @param string $json
     * @param string $className
     * @test
     * @dataProvider providerJson
     */
    public function canDeserialize(string $json, string $className): void
    {
        self::$logger->info('Deserializing: '.$className.' - '.print_r($json, true));
        $stdClass = DeserializerHelper::parseJSON($json, $className);
        self::$logger->info(print_r($stdClass, true));
        self::assertInstanceOf($className, $stdClass);
    }

    /**
     * @return array
     */
    public function providerJson(): array
    {
        $arr = $arr1 = [
            'foo' => 'foo',
            'bar' => 'bar',
        ];

        $arr2 = [
            'foo' => 'foobar',
            'bar' => 'barfoo',
        ];

        $arr1['bar'] = $arr2;

        return [
            ['{}', 'stdClass'],
            [json_encode($arr), 'stdClass'],
            [json_encode($arr1), 'stdClass'],
            [json_encode($arr), Dummy::class],
            [json_encode($arr), Foo::class],
            [json_encode($arr), Bar::class],
            [json_encode($arr1), DummyFull::class],
        ];
    }

    /**
     * @return array|\string[][]
     */
    public function providerNoJson(): array
    {
        return [['{"No JSON":"wrong}', 'stdClass']];
    }

    /**
     * @param string $noXML
     * @param string $className
     * @test
     * @dataProvider providerNoXML
     */
    public function cantDeserializeNoXML(string $noXML, string $className): void
    {
        self::expectException(InvalidArgumentException::class);
        self::$logger->info('Deserializing: '.$className.' - '.print_r($noXML, true));
        DeserializerHelper::parseXML($noXML, $className);
    }

    /**
     * @param string $xml
     * @param string $className
     * @test
     * @dataProvider providerXML
     */
    public function canDeserializeXML(string $xml, string $className): void
    {
        self::$logger->info('Deserializing: '.$className.' - '.print_r($xml, true));
        $object = DeserializerHelper::parseXML($xml, $className);
        self::$logger->info(print_r($object, true));
        self::assertInstanceOf($className, $object);
    }

    /**
     * @return array|\string[][]
     */
    public function providerXML(): array
    {
        $xml1 = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<Dummy>
   <foo>foo</foo>
   <bar>bar</bar>
</Dummy>
XML;
        $xml2 = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<Foo>
   <foo>foo</foo>
</Foo>
XML;

        $xml3 = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<Bar>
   <bar>bar</bar>
</Bar>
XML;

        $xml4 = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<DummyFull>
   <foo>foo</foo>
   <bar>bar</bar>
</DummyFull>
XML;

        return [
            [$xml1, Dummy::class],
            [$xml2, Foo::class],
            [$xml3, Bar::class],
            [$xml4, DummyFull::class],
        ];
    }

    /**
     * @return array|\string[][]
     */
    public function providerNoXML(): array
    {
        return [['{"No XML":"wrong}', Dummy::class]];
    }
}
