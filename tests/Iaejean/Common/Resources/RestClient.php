<?php

declare(strict_types=1);

namespace Tests\Iaejean\Common\Resources;

use Iaejean\Common\HttpClient\AbstractClient;
use Iaejean\Common\HttpClient\Exceptions\CommunicationException;
use Iaejean\Common\HttpClient\Exceptions\RestException;
use Iaejean\Common\HttpClient\Request\Multipart;
use Iaejean\Common\HttpClient\Request\RestRequest;
use Iaejean\Common\HttpClient\Response\Response;

/**
 * Class RestClient
 * @package Tests\Iaejean\Common\Resources
 */
class RestClient extends AbstractClient
{
    /**
     * @var string
     */
    private string $url = 'https://jsonplaceholder.typicode.com';

    /**
     * @throws CommunicationException
     * @throws RestException
     * @return Response
     */
    public function getPosts(): Response
    {
        $request = new RestRequest($this->url, 'posts');

        return $this->get($request);
    }

    /**
     * @throws CommunicationException
     * @throws RestException
     * @return Response
     */
    public function postPosts(): Response
    {
        $request = new RestRequest($this->url, 'posts');
        $request->setBody('{
          "userId": 1,
          "id": 1,
          "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
          "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit"
        }');

        return $this->post($request);
    }

    /**
     * @throws CommunicationException
     * @throws RestException
     * @return Response
     */
    public function postPostMultipart(): Response
    {
        $request = new RestRequest($this->url, 'posts');
        $request->setMultipart([new Multipart('name', __FILE__, 'name.php')]);

        return $this->post($request);
    }

    /**
     * @throws CommunicationException
     * @throws RestException
     * @return Response
     */
    public function putPost(): Response
    {
        $request = new RestRequest($this->url, 'posts/1');
        $request->setBody('{
          "userId": 1,
          "id": 1,
          "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
          "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit"
        }');

        return $this->put($request);
    }

    /**
     * @throws CommunicationException
     * @throws RestException
     * @return Response
     */
    public function deletePost(): Response
    {
        $request = new RestRequest($this->url, 'posts/1');

        return $this->delete($request);
    }
}
