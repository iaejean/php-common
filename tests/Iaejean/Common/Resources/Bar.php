<?php

declare(strict_types=1);

namespace Tests\Iaejean\Common\Resources;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Bar
 * @package Tests\Iaejean\Common\Resources
 */
class Bar
{
    /**
     * @Serializer\Type("string")
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @var string|null
     */
    private ?string $bar;

    /**
     * Bar constructor.
     * @param string|null $bar
     */
    public function __construct(string $bar = null)
    {
        $this->bar = $bar;
    }
}
