<?php

declare(strict_types=1);

namespace Tests\Iaejean\Common\Resources;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class DummyFull
 * @package Tests\Iaejean\Common\Resources
 */
class DummyFull
{
    /**
     * @Serializer\Type("string")
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @var string|null
     */
    private ?string $foo;

    /**
     * @Serializer\Type("Tests\Iaejean\Common\Resources\Dummy")
     * @Assert\NotBlank
     * @Assert\Valid
     * @var Dummy|null
     */
    private ?Dummy $bar;

    /**
     * DummyFull constructor.
     * @param null $foo
     * @param Dummy|null $bar
     */
    public function __construct($foo = null, Dummy $bar = null)
    {
        $this->foo = $foo;
        $this->bar = $bar;
    }
}
