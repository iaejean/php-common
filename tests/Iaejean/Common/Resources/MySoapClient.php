<?php

declare(strict_types=1);

namespace Tests\Iaejean\Common\Resources;

use Iaejean\Common\HttpClient\AbstractClient;
use Iaejean\Common\HttpClient\Exceptions\CommunicationException;
use Iaejean\Common\HttpClient\Exceptions\SoapException;
use Iaejean\Common\HttpClient\Request\SoapRequest;
use Iaejean\Common\HttpClient\Response\Response;

/**
 * Class MySoapClient
 * @package Tests\Iaejean\Common\Resources
 */
class MySoapClient extends AbstractClient
{
    /**
     * @var string
     */
    private string $url = 'http://www.webservicex.net/globalweather.asmx?WSDL';

    /**
     * @throws CommunicationException
     * @throws SoapException
     * @return Response
     */
    public function getCall(): Response
    {
        $request = new SoapRequest(
            $this->url,
            'GetCitiesByCountry',
            ['GetCitiesByCountrySoapIn' => ['CountryName' => 'Mexico']]
        );

        return $this->call($request);
    }
}
