<?php

declare(strict_types=1);

namespace Tests\Iaejean\Common\Resources;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Dummy
 * @package Tests\Iaejean\Common\Resources
 */
class Dummy
{
    /**
     * @Serializer\Type("string")
     * @Serializer\XmlElement(cdata=false)
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @var string|null
     */
    private ?string $foo;

    /**
     * @Serializer\Type("string")
     * @Serializer\XmlElement(cdata=false)
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @var string|null
     */
    private ?string $bar;

    /**
     * Dummy constructor.
     * @param string|null $foo
     * @param string|null $bar
     */
    public function __construct(string $foo = null, string $bar = null)
    {
        $this->bar = $bar;
        $this->foo = $foo;
    }
}
