<?php

declare(strict_types=1);

namespace Tests\Iaejean\Common\Resources;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Foo
 * @package Tests\Iaejean\Common\Resources
 */
class Foo
{
    /**
     * @Serializer\Type("string")
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @var string|null
     */
    private ?string $foo;

    /**
     * Foo constructor.
     * @param string|null $foo
     */
    public function __construct(string $foo = null)
    {
        $this->foo = $foo;
    }
}
