<?php

declare(strict_types=1);

use Iaejean\Common\Helpers\DeserializerHelper;
use Iaejean\Common\Helpers\SerializerHelper;
use Iaejean\Common\Helpers\ValidatorHelper;
use Symfony\Component\Translation\Translator;

if (!defined('PHPUNIT_COMPOSER_INSTALL')) {
    define('PHPUNIT_COMPOSER_INSTALL', __DIR__.'/../../../vendor/autoload.php');
}

if (!defined('CACHE_DIRECTORY')) {
    define('CACHE_DIRECTORY', __DIR__.'/../../../cache');
}

if (is_dir(CACHE_DIRECTORY)) {
    $files = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator(CACHE_DIRECTORY, RecursiveDirectoryIterator::SKIP_DOTS),
        RecursiveIteratorIterator::CHILD_FIRST
    );

    foreach ($files as $fileInfo) {
        $todo = ($fileInfo->isDir() ? 'rmdir' : 'unlink');
        $todo($fileInfo->getRealPath());
    }

    rmdir(CACHE_DIRECTORY);
}

mkdir(CACHE_DIRECTORY);
DeserializerHelper::$cacheDir = CACHE_DIRECTORY;
SerializerHelper::$cacheDir = CACHE_DIRECTORY;
ValidatorHelper::$cacheDir = CACHE_DIRECTORY;
ValidatorHelper::$translator = new Translator('es');
